import winston from 'winston';

/**
 * Initialize logger which just logs into console
 */
const logger = winston.createLogger({
  level: 'debug',
  format: winston.format.simple(),
  transports: [
    new winston.transports.Console({
      format: winston.format.simple(),
      silent: true,
    }),
  ],
});

logger.stream = {
  write: message => {
    if (process.env.NODE_ENV !== 'test') {
      logger.info(message);
    }
  },
};

export default logger;
