const http = require('http');
const path = require('path');

if (process.env.NODE_ENV !== 'development') {
  var socket = require('./dist/api/modules/Socket');
} else {
  socket = require('./src/api/modules/Socket');
}
require('dotenv').config({
  path: path.join(
    process.cwd(),
    process.env.NODE_ENV !== 'development' ? '.env.prod' : '.env.dev',
  ),
});

process.on('uncaughtException', err => {
  console.log('err', err);
  console.log(`Caught exception: ${err}`);
});
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise ', p, 'reason:', reason);
});
process.on('SIGTERM', err => {
  console.log(`Caught exception: ${err}`);
});
process.on('warning', warning => {
  console.warn(warning.name);
  console.warn(warning.message);
  console.warn(warning.stack);
});

const requestHandler = (request, response) => {
  console.log(request.url);
  response.end('Hello Socket Server!');
};

const server = http.createServer(requestHandler);
socket(server);
server.listen(process.env.PORT_SOCKET, '0.0.0.0', () => {
  console.info(
    'api listening at http://%s:%s',
    server.address().address,
    server.address().port,
  );
});
