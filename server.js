require('babel-polyfill');
var http = require('http');
// eslint-disable-next-line vars-on-top
var path = require('path');
var models = require('./db/models');
require('dotenv').config({
  path: path.join(
    process.cwd(),
    process.env.NODE_ENV !== 'development' ? '.env.prod' : '.env.dev',
  ),
});
process.on('uncaughtException', err => {
  console.log(`Caught exception: ${err}`);
});
process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise ', p, 'reason:', reason);
});
process.on('SIGTERM', err => {
  console.log(`Caught exception: ${err}`);
});
process.on('warning', (warning) => {
  console.warn(warning.name);
  console.warn(warning.message);
  console.warn(warning.stack);
});
if (process.env.NODE_ENV !== 'development') {
  var App = require('./dist/app').default;
} else {
  App = require('./src/app').default;
}

var server = http.createServer(App());
models.sequelize.sync()
  .then(() => {
    server.listen(process.env.PORT, '0.0.0.0', () => {
      console.info(
        'api listening at http://%s:%s',
        server.address().address,
        server.address().port,
      );
    });
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

process.on('exit', code => {
  setTimeout(() => {
    console.error(`This will not run ${code}`);
  }, 0);
});



