import _ from 'lodash';
import jwt from 'jsonwebtoken';
import responseHandler from '../../../response';
import crypto from 'crypto';
import UserModel from '../User/model';
import email from '../../helpers/email';
// import * as userConstants from '../../../constants/user';
import { AuthError } from '../../helpers/errors';

export default class AuthController {
  static async signIn(req, res, next) {
    req
      .checkBody('email', 'Email не является допустимым')
      .isEmail()
      .trim();
    req
      .checkBody('password', 'Пароль не может быть пустым.')
      .notEmpty()
      .trim();
    req
      .checkBody('password', 'Password must be longer than 6 characters.')
      .len({ min: 6 });
    try {
      await req.asyncValidationErrors();
      const result = await UserModel.signIn(
        req.body.email.trim(),
        req.body.password.trim(),
      );
      return responseHandler(res, 'SIGNED_IN', result);
    } catch (error) {
      return next(error);
    }
  }
  static async singUp(req, res, next) {
    req.sanitizeBody('email').normalizeEmail({
      gmail_remove_dots: false,
    });
    // req
    //   .checkBody('name', 'Name is required.')
    //   .notEmpty()
    //   .trim();
    req
      .checkBody('email', 'Email is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('password', 'Password is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('password', 'Password must be longer than 6 characters.')
      .len({ min: 6 });
    // req.checkBody('confirmpass', 'Password is required.').notEmpty();
    // req.checkbody('confirmpass', 'Password do not match').equals(req.body.password);
    try {
      await req.asyncValidationErrors();
      const user = await UserModel.singUp(req.body);
      email(req.body.email, 'email', {
        key: user.confirm,
      });
      return responseHandler(res, 'SIGNED_UP');
    } catch (error) {
      return next(error);
    }
  }
  static async refreshToken(req, res, next) {
    req.checkBody('refreshToken', 'Incorrect refreshToken address').notEmpty();
    req.checkBody('accessToken', 'Incorrect accessToken address').notEmpty();
    try {
      await req.asyncValidationErrors();
      const { refreshToken, accessToken } = req.body;
      const data = await UserModel.refreshToken(accessToken, refreshToken);
      return responseHandler(res, 'SUCCESS', data);
    } catch (error) {
      return next(error);
    }
  }
  static async getByKey(req, res, next) {
    // req.checkBody('key', 'key cannot be blank.').notEmpty();
    const { key, forgot } = req.params;
    try {
      const user = await UserModel.getUserByKey(key);
      if (!_.isEmpty(user)) {
        await UserModel.update(user.id, { confirm: null });
        if (forgot) {
          const accessToken = jwt.sign(
            {
              id: user.id,
              email: user.email,
            },
            process.env.JWT_SECRET,
            {
              expiresIn: process.env.ACCESS_TOKEN_TTL,
            },
          );
          return res.redirect(`rerid://forgot/${accessToken}`);
        }
        return res.redirect('rerid://login');
        // return responseHandler(res, 'SUCCESS');
      }
      throw new AuthError('User not Exist');
    } catch (error) {
      return next(error);
    }
  }
  static async forgotPassword(req, res, next) {
    req.checkBody('email', 'Email cannot be blank.').notEmpty();
    req.checkBody('email', 'Email is not valid.').isEmail();
    req.sanitizeBody('email').normalizeEmail({ gmail_remove_dots: false });
    try {
      await req.asyncValidationErrors();
      const user = await UserModel.getUserByEmail(req.body.email);
      if (!_.isEmpty(user)) {
        if (user.removed) {
          throw new AuthError('User is blocked');
        }
        const confirmKey = crypto
          .createHash('sha1')
          .update(user.email, user.id)
          .digest('hex');
        email(req.body.email, 'forgot', {
          firstName: user.name,
          email: user.email,
          key: confirmKey,
          forgot: true,
        });
        await UserModel.update(user.id, { confirm: confirmKey });
        return responseHandler(res, 'SUCCESS');
      }
      throw new AuthError('User not Exist');
    } catch (error) {
      return next(error);
    }
  }
}
