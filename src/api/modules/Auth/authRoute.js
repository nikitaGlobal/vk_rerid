import container from './authController';
import { NotSignin } from '../../helpers/middleware/auth';

export default router => {
  router.post('/signin', NotSignin, container.signIn);
  router.post('/signup', NotSignin, container.singUp);
  router.post('/token', NotSignin, container.refreshToken);
  router.post('/forgot', NotSignin, container.forgotPassword);
  router.get('/key/:key/:forgot?', NotSignin, container.getByKey);
};
