import multer from '../../helpers/multer';
import fs from 'fs';

const upload = multer.array('file', 12);

export default class ImageContainer {
  // eslint-disable-next-line no-unused-vars
  static async attachments(req, res, next) {
    const images = [];
    // eslint-disable-next-line consistent-return
    upload(req, res, err => {
      if (err) {
        res.send({ error: err });
      } else {
        if (req.files === undefined) {
          return res.json({ msg: 'Error: No File Selected!' });
        }
        req.files.forEach(async value => {
          images.push({
            filename: value.filename,
            originalName: value.originalname,
          });
        });
        return res.json({
          msg: 'File Uploaded!',
          image: images,
        });
      }
    });
  }
  static async removeAttachments(req, res) {
    fs.unlink(`assets/upload/${req.body.filename}`, err => {
      if (err) {
        res.json(`failed to delete local image:${err}`);
      } else {
        res.json({
          success: 'true',
          massages: 'successfully deleted local image',
        });
      }
    });
  }
}
