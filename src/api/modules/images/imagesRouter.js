import container from './imageContainer';
import responseHandler from '../../../response';
import fs from 'fs';
import path from 'path';

export default router => {
  router.post('/attachments', container.attachments);
  router.post('/removeAttachments', container.removeAttachments);
  // eslint-disable-next-line consistent-return
  router.get('/:file', (req, res) => {
    const { file } = req.params;
    const imgPath = path.join(__dirname, '../../../../assets/upload', file);
    if (fs.existsSync(imgPath)) {
      const img = fs.readFileSync(imgPath);
      res.writeHead(200, {
        'Content-Type': 'image/jpg',
      });
      res.end(img, 'binary');
    } else {
      return responseHandler(res, 'NOT_FOUND', null, {});
    }
  });
  router.get('/audio/:file', (req, res) => {
    const { file } = req.params;
    const fullPath = path.join(__dirname, '../../../../assets/upload', file);
    const start = fs.statSync(fullPath);
    res.writeHead(200, {
      'Content-Type': 'audio/aac',
      'Content-Length': start.size,
    });
    const readStream = fs.createReadStream(fullPath);
    // readStream.setEncoding('utf8');
    // return readStream;
    readStream.pipe(res);
  });
  router.get('/video/:file', (req, res) => {
    const { file } = req.params;
    const fullPath = path.join(__dirname, '../../../../assets/upload', file);
    const stat = fs.statSync(fullPath);
    const fileSize = stat.size;
    // eslint-disable-next-line prefer-destructuring
    const range = req.headers.range;
    // eslint-disable-next-line prefer-destructuring
    if (range) {
      const parts = range.replace(/bytes=/, '').split('-');
      const start = parseInt(parts[0], 10);
      const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;

      const chunksize = end - start + 1;
      const result = fs.createReadStream(fullPath, { start, end });
      const head = {
        'Accept-Ranges': 'bytes',
        'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        'Content-Length': chunksize,
        // 'Content-Type': 'video/mp4',
      };

      res.writeHead(206, head);
      result.pipe(res);
    } else {
      res.writeHead(200, {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
      });
      fs.createReadStream(fullPath).pipe(res);
    }
  });
  // eslint-disable-next-line consistent-return
  router.get('/pdf/:file/:name', (req, res) => {
    const { file, name } = req.params;
    const pdf = path.join(__dirname, '../../../../assets/upload', file);
    if (fs.existsSync(pdf)) {
      res.download(pdf, name, err => {
        if (err) {
          console.log(err);
        } else {
          console.log('Success Upload');
        }
      });
      // res.end(img, 'binary');
    } else {
      return responseHandler(res, 'NOT_FOUND', null, {});
    }
  });
};
