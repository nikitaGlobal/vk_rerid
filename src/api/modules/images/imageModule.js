import Routes from './imagesRouter';
import * as express from 'express';

class ImageModule {
  constructor(apiRouter) {
    this.router = express.Router();
    this.apiRouter = apiRouter;
  }
  createEndpoints() {
    this.assignRouter();
    this.assignEndpoints();
  }
  assignRouter() {
    this.apiRouter.use('/upload', this.router);
  }
  assignEndpoints() {
    Routes(this.router);
  }
}

export default apiRouter => new ImageModule(apiRouter);
