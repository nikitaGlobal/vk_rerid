import _ from 'lodash';
import responseHandler from '../../../response';
import articleModel from './model';
import pubModel from '../Publications/model';
import { ValidationError } from '../../helpers/errors';
import * as permissions from '../../../constants/user';
import { WAITING } from '../../../constants/article';

export default class ArticlesController {
  static async allArticles(req, res, next) {
    try {
      const { type } = req.query;

      const data = await articleModel.getAllArticles(type);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async articlesByPubId(req, res, next) {
    try {
      const { pubId, type } = req.query;
      if (_.isEmpty(pubId)) {
        throw new ValidationError('Id not Defined');
      }
      const data = await articleModel.getByPubId(pubId, type, req.user.id);
      const articles = [];
      let EDITOR = false;
      let CORRECTOR = false;
      let LAWYER = false;
      if (data.articles) {
        data.articles.forEach(value => {
          EDITOR =
            // eslint-disable-next-line no-bitwise
            (value.status & Number(permissions.EDITOR)) ===
            Number(permissions.EDITOR);
          CORRECTOR =
            // eslint-disable-next-line no-bitwise
            (value.status & Number(permissions.CORRECTOR)) ===
            Number(permissions.CORRECTOR);
          LAWYER =
            // eslint-disable-next-line no-bitwise
            (value.status & Number(permissions.LAWYER)) ===
            Number(permissions.LAWYER);
          articles.push({
            ...value.get({ plain: true }),
            LAWYER,
            CORRECTOR,
            EDITOR,
          });
        });
      }
      return responseHandler(res, 'SUCCESS', articles);
    } catch (err) {
      console.log('err', err);
      return next(err);
    }
  }
  static async getArticleById(req, res, next) {
    try {
      const { id } = req.params;
      if (_.isEmpty(id)) {
        throw new ValidationError('Id not Defined');
      }
      const data = await articleModel.getById(id);
      const EDITOR =
        // eslint-disable-next-line no-bitwise
        (data.get({ plain: true }).status & Number(permissions.EDITOR)) ===
        Number(permissions.EDITOR);
      const CORRECTOR =
        // eslint-disable-next-line no-bitwise
        (data.get({ plain: true }).status & Number(permissions.CORRECTOR)) ===
        Number(permissions.CORRECTOR);
      const LAWYER =
        // eslint-disable-next-line no-bitwise
        (data.get({ plain: true }).status & Number(permissions.LAWYER)) ===
        Number(permissions.LAWYER);
      return responseHandler(res, 'SUCCESS', {
        ...data.get({ plain: true }),
        EDITOR,
        CORRECTOR,
        LAWYER,
      });
    } catch (err) {
      return next(err);
    }
  }
  static async changeStatus(req, res, next) {
    req
      .checkBody('artId', 'artId is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await articleModel.getById(req.body.artId);
      if (!_.isEmpty(data)) {
        const user = await pubModel.getPubIdAndUserId(data.pubId, req.user.id);
        // eslint-disable-next-line no-bitwise
        if ((data.status & Number(user.role)) !== Number(user.role)) {
          const count = Number(user.role) + data.status;
          const update = await articleModel.update(data.id, { status: count });
          return responseHandler(res, 'SUCCESS', update);
        }
        throw new ValidationError('Not Access');
      }
      throw new ValidationError('data not Defined');
    } catch (err) {
      return next(err);
    }
  }
  static async getDraft(req, res, next) {
    const { pubId } = req.params;
    try {
      if (_.isEmpty(pubId)) {
        throw new ValidationError('Pub Id not Defined');
      }
      const draft = await articleModel.getDraftByUser(pubId, req.user.id);
      return responseHandler(res, 'SUCCESS', draft);
    } catch (err) {
      return next(err);
    }
  }
  static async create(req, res, next) {
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('pubId', 'pubId is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = {
        name: req.body.name,
        description: req.body.description,
        pubId: req.body.pubId,
        type: req.body.type || WAITING,
        userId: req.user.id,
      };
      const article = await articleModel.create(data);
      if (req.body.files) {
        const files = [];
        req.body.files.forEach(element => {
          if (element.type === 'audio') {
            files.push(
              articleModel.audiosCreate({
                filename: element.filename,
                artId: article.id,
              }),
            );
          } else {
            files.push(
              articleModel.articleImagesCreate({
                filename: element.filename,
                originalName: element.originalName,
                artId: article.id,
              }),
            );
          }
        });
        await Promise.all(files);
      }
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async update(req, res, next) {
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('id', 'name is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await articleModel.update(req.body.id, {
        name: req.body.name,
        description: req.body.description,
        type: WAITING,
      });
      if (req.body.files.audios) {
        await articleModel.AudiosDestroyByArId(req.body.id);
        const audios = [];
        req.body.files.audios.forEach(element => {
          audios.push(
            articleModel.audiosCreate({
              filename: element.filename,
              artId: req.body.id,
            }),
          );
        });
        await Promise.all(audios);
      }
      if (req.body.files.images) {
        await articleModel.ImagesDestroyByArId(req.body.id);
        const images = [];
        req.body.files.images.forEach(element => {
          images.push(
            articleModel.articleImagesCreate({
              filename: element.filename,
              originalName: element.originalName,
              artId: req.body.id,
            }),
          );
        });
        await Promise.all(images);
      }
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async updateType(req, res, next) {
    const { id } = req.params;
    try {
      const data = await articleModel.update(id, { type: req.body.type });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async destroy(req, res, next) {
    try {
      const { id } = req.params;
      const data = await articleModel.destroy(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async getCommentByIdAndUserId(req, res, next) {
    try {
      const { id } = req.params;
      const data = await articleModel.getCommentByIdAndUserId(id, req.user.id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      console.log(JSON.stringify(err, null, 4));
      return next(err);
    }
  }
  static async getComments(req, res, next) {
    try {
      const data = await articleModel.getCommentsByUser(req.user.id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      console.log(JSON.stringify(err));
      return next(err);
    }
  }
  static async answer(req, res, next) {
    req
      .checkBody('to', 'name is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('comId', 'name is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('comment', 'name is required.')
      .notEmpty()
      .trim();
    try {
      const data = await articleModel.createCommentAnswer({
        from: req.user.id,
        to: req.body.to,
        comment: req.body.comment,
        comId: req.body.comId,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      console.log(JSON.stringify(err));
      return next(err);
    }
  }
}
