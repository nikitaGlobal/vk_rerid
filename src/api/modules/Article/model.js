import Models from '../../../../db/models';
import { DRAFT } from '../../../constants/article';

export default class ArticlesModelService {
  static async getAllArticles(type) {
    try {
      const pub = Models.article.findAll({
        where: { removed: false, type },
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async create(data) {
    try {
      const pub = Models.article.create(data);
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async update(id, data) {
    try {
      const update = Models.article.update(data, {
        where: { id, removed: false },
      });
      return update;
    } catch (err) {
      throw err;
    }
  }
  static async getById(id) {
    try {
      const user = await Models.article.findOne({
        where: { id, removed: false },
        include: [
          {
            required: false,
            model: Models.articleImages,
            raw: true,
          },
          {
            required: false,
            model: Models.articleAudios,
            raw: true,
          },
          {
            required: false,
            model: Models.articleComment,
            as: 'comment',
            raw: true,
            include: [
              {
                required: false,
                model: Models.users,
                raw: true,
              },
            ],
          },
        ],
      });
      return user;
    } catch (err) {
      throw err;
    }
  }
  static async getByPubId(id, type, userId) {
    let where = {
      removed: false,
      '$articles.type$': {
        [Models.sequelize.Op.not]: DRAFT,
      },
    };
    if (type) {
      where = {
        removed: false,
        '$articles.type$': type,
      };
    } else if (type && type === DRAFT) {
      where = {
        removed: false,
        '$articles.type$': type,
        '$articles.userId$': userId,
      };
    }
    try {
      const res = await Models.publications.findOne({
        where: { id, removed: false },
        attributes: ['id'],
        include: [
          {
            required: false,
            model: Models.article,
            where,
            as: 'articles',
          },
        ],
      });
      return res;
    } catch (err) {
      throw err;
    }
  }
  static async getDraftByUser(pubId, userId) {
    try {
      const res = await Models.article.findOne({
        where: { pubId, userId, type: DRAFT, removed: false },
      });
      return res;
    } catch (err) {
      throw err;
    }
  }
  static async getArticlesByPubId(pubId) {
    try {
      const article = await Models.article.findOne({
        where: { pubId },
      });
      return article;
    } catch (err) {
      throw err;
    }
  }
  static async destroy(id) {
    try {
      const data = await Models.article.destroy(
        { removed: true },
        { where: { id } },
      );
      return data;
    } catch (err) {
      throw err;
    }
  }
  static async AudiosDestroyByArId(artId) {
    try {
      const data = await Models.articleAudios.destroy({ where: { artId } });
      return data;
    } catch (err) {
      throw err;
    }
  }
  static async ImagesDestroyByArId(artId) {
    try {
      const data = await Models.articleImages.destroy({ where: { artId } });
      return data;
    } catch (err) {
      throw err;
    }
  }
  static async audiosCreate(data) {
    try {
      const audios = Models.articleAudios.create(data);
      return audios;
    } catch (err) {
      throw err;
    }
  }

  static async articleImagesCreate(data) {
    try {
      const images = Models.articleImages.create(data);
      return images;
    } catch (err) {
      throw err;
    }
  }
  static async createComment(data, transaction = null) {
    try {
      const res = Models.comment.create(data, { transaction });
      return res;
    } catch (err) {
      throw err;
    }
  }
  static async createCommentAnswer(data, transaction = null) {
    try {
      const res = Models.answerComments.create(data, { transaction });
      return res;
    } catch (err) {
      throw err;
    }
  }
  static async getCommentByIdAndUserId(id, userId) {
    try {
      const comment = await Models.comment.findOne({
        where: {
          id,
          [Models.sequelize.Op.or]: [{ to: userId }, { from: userId }],
        },
        include: [
          {
            model: Models.users,
            as: 'send',
            // include: [
            //   {
            //     model: Models.pubUser,
            //     include: [
            //       {
            //         model: Models.publications,
            //       },
            //     ],
            //   },
            // ],
          },
          {
            required: false,
            model: Models.answerComments,
            as: 'answers',
          },
          {
            required: false,
            model: Models.pubUser,
            as: 'info',
            include: [
              {
                model: Models.publications,
              },
            ],
          },
          {
            required: false,
            model: Models.users,
            as: 'invited_user',
            // include: [
            //   {
            //     required: false,
            //     model: Models.comment,
            //     where: { status: false },
            //     attributes: ['id', 'comment', 'status'],
            //     as: 'comment',
            //     // include: [
            //     //   {
            //     //     required: false,
            //     //     model: Models.answerComments,
            //     //     as: 'answers',
            //     //   },
            //     // ],
            //   },
            // ],
          },
        ],
      });
      return comment;
    } catch (err) {
      throw err;
    }
  }
  static async getCommentsByUser(id) {
    // console.log('to', to);
    try {
      const comment = await Models.comment.findAll({
        where: {
          [Models.sequelize.Op.or]: [{ to: id }, { from: id }],
        },
        include: [
          {
            model: Models.users,
            as: 'send',
            include: [
              {
                model: Models.pubUser,
                include: [
                  {
                    model: Models.publications,
                  },
                ],
              },
            ],
          },
          {
            required: false,
            model: Models.pubUser,
            as: 'info',
            include: [
              {
                model: Models.publications,
              },
            ],
          },
          {
            required: false,
            model: Models.users,
            as: 'invited_user',
            // include: [
            //   {
            //     required: false,
            //     model: Models.comment,
            //     where: { status: false },
            //     attributes: ['id', 'comment', 'status'],
            //     as: 'comment',
            //     // include: [
            //     //   {
            //     //     required: false,
            //     //     model: Models.answerComments,
            //     //     as: 'answers',
            //     //   },
            //     // ],
            //   },
            // ],
          },
        ],
      });
      return comment;
    } catch (err) {
      console.log('err', JSON.stringify(err));
      throw err;
    }
  }
}
