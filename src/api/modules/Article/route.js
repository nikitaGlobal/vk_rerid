import container from './controller';
import {
  checkAuthenticationToken,
  checkPermissionUser,
} from '../../helpers/middleware/auth';
import * as permissions from '../../../constants/user';

export default router => {
  router.use(checkAuthenticationToken);
  router.get('/all', container.allArticles);
  router.get('/', container.articlesByPubId);
  router.post(
    '/create',
    checkPermissionUser([permissions.JOURNALIST]),
    container.create,
  );
  router.put(
    '/update-type/:id',
    checkPermissionUser([permissions.JOURNALIST, permissions.EDITOR]),
    container.updateType,
  );
  router.post(
    '/change-status',
    checkPermissionUser([
      permissions.EDITOR,
      permissions.CORRECTOR,
      permissions.LAWYER,
    ]),
    container.changeStatus,
  );
  router.get('/id/:id', container.getArticleById);
  router.get('/draft/:pubId', container.getDraft);

  router.put(
    '/update',
    checkPermissionUser([
      permissions.EDITOR,
      permissions.CORRECTOR,
      permissions.JOURNALIST,
    ]),
    container.update,
  );
  router.get('/comment/:id', container.getCommentByIdAndUserId);
  router.post('/comment-answer', container.answer);
  router.get('/comments', container.getComments);
  router.get('/answer', container.getComments);
  router.delete(
    '/destroy/:id',
    checkPermissionUser([permissions.JOURNALIST]),
    container.destroy,
  );
};
