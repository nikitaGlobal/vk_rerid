import Models from '../../../../db/models';

export default class FavoriteModelService {
  static async getAll() {
    try {
      const cat = Models.favorite.findAll({
        where: { removed: false },
      });
      return cat;
    } catch (err) {
      throw err;
    }
  }
  static async getAllByUser(userId) {
    try {
      const data = Models.favorite.findAll({
        where: { removed: false, userId },
        attributes: ['id', 'pubId'],
        include: [
          {
            model: Models.publications,
            attributes: ['id', 'name', 'description', 'avatar', 'createdAt'],
            raw: true,
          },
        ],
      });
      return data;
    } catch (err) {
      throw err;
    }
  }
  static async getByPubId(pubId) {
    try {
      const favorite = Models.favorite.findOne({
        where: { removed: false, pubId },
      });
      return favorite;
    } catch (err) {
      throw err;
    }
  }
  static async create(data) {
    try {
      const favorite = Models.favorite.create(data);
      return favorite;
    } catch (err) {
      throw err;
    }
  }
  static async update(id, data) {
    try {
      const update = Models.favorite.update(data, { where: { id } });
      return update;
    } catch (err) {
      throw err;
    }
  }
  static async getById(id) {
    try {
      const user = await Models.favorite.findOne({
        where: { id, removed: false },
        raw: true,
      });
      return user;
    } catch (err) {
      throw err;
    }
  }
  static async destroy(id) {
    try {
      const data = await Models.favorite.update(
        { removed: true },
        { where: { id } },
      );
      return data;
    } catch (err) {
      throw err;
    }
  }
  static async destroyByPubId(pubId) {
    try {
      const data = await Models.favorite.destroy({ where: { pubId } });
      return data;
    } catch (err) {
      throw err;
    }
  }
}
