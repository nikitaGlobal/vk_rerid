import _ from 'lodash';
import responseHandler from '../../../response';
import favoriteModel from './model';
import { ValidationError } from '../../helpers/errors';

export default class PopularController {
  static async getAll(req, res, next) {
    try {
      const data = await favoriteModel.getAll();
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async getAllByUser(req, res, next) {
    try {
      const data = await favoriteModel.getAllByUser(req.user.id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      console.log('err', err);
      return next(err);
    }
  }
  static async getById(req, res, next) {
    try {
      const { id } = req.params;
      if (_.isEmpty(id)) {
        throw new ValidationError('Id not Defined');
      }
      const data = await favoriteModel.getById(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async create(req, res, next) {
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await favoriteModel.create(req.body);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async favorite(req, res, next) {
    req
      .checkBody('pubId', 'pubId is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await favoriteModel.getByPubId(req.body.pubId);
      if (data) {
        await favoriteModel.destroyByPubId(req.body.pubId);
        return responseHandler(res, 'SUCCESS', { type: 'delete' });
      }
      await favoriteModel.create({
        pubId: req.body.pubId,
        userId: req.user.id,
      });
      return responseHandler(res, 'SUCCESS', { type: 'create' });
    } catch (err) {
      return next(err);
    }
  }
  static async update(req, res, next) {
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await favoriteModel.update(req.body.id, {
        name: req.body.name,
        avatar: req.body.avatar,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async destroy(req, res, next) {
    try {
      const { id } = req.params;
      const data = await favoriteModel.destroy(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
}
