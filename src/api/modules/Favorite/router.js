import container from './container';
import { checkAuthenticationToken } from '../../helpers/middleware/auth';

export default router => {
  router.use(checkAuthenticationToken);
  router.get('/', container.getAllByUser);
  router.post(
    '/',
    // checkRoleUser(permission.ADMINISTRATOR),
    container.favorite,
  );
  router.post(
    '/create',
    // checkRoleUser(permission.ADMINISTRATOR),
    container.create,
  );
  router.get('/id/:id', container.getById);
  router.put('/', container.update);
  router.delete('/:id', container.destroy);
};
