import _ from 'lodash';
import responseHandler from '../../../response';
import Models from '../../../../db/models';
import pubModel from './model';
import articleModel from '../Article/model';
import { ValidationError } from '../../helpers/errors';
import * as constants from '../../../constants/user';

export default class PublicationsController {
  static async allPublications(req, res, next) {
    try {
      const data = await pubModel.getAllPublications();
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async getPublicationsById(req, res, next) {
    try {
      const { id } = req.params;
      if (_.isEmpty(id)) {
        throw new ValidationError('Id not Defined');
      }
      const data = await pubModel.getById(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      console.log('err', err);
      return next(err);
    }
  }
  static async create(req, res, next) {
    req
      .checkBody('name', 'userId is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('catId', 'catId is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      req.body.userId = req.user.id;
      const data = await pubModel.create(req.body);
      await pubModel.createPubUser({
        userId: req.user.id,
        pubId: data.id,
        role: constants.EDITOR,
        status: true,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async invitePubUser(req, res, next) {
    req
      .checkBody('userId', 'userId is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('pubId', 'catId is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('role', 'catId is required.')
      .notEmpty()
      .trim();
    let transaction;
    try {
      transaction = await Models.sequelize.transaction();
      const pubUser = await pubModel.getByUserIdAndRole(
        req.body.userId,
        req.body.pubId,
        req.body.role,
      );
      if (pubUser) {
        throw new ValidationError('пользователь уже есть');
      }
      const data = await pubModel.createPubUser(
        {
          userId: req.body.userId,
          pubId: req.body.pubId,
          role: req.body.role,
        },
        transaction,
      );
      await articleModel.createComment(
        {
          from: req.user.id,
          to: req.body.userId,
          pubUserId: data.id,
          comment: req.body.comment,
        },
        transaction,
      );
      await transaction.commit();
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      await transaction.rollback();
      return next(err);
    }
  }
  static async update(req, res, next) {
    req
      .checkBody('id', 'id is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('catId', 'catId is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await pubModel.update(req.body.id, req.user.id, {
        name: req.body.name,
        description: req.body.description,
        catId: req.body.catId,
        avatar: req.body.avatar,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async changeVisible(req, res, next) {
    const { id } = req.params;
    try {
      await req.asyncValidationErrors();
      const data = await pubModel.update(id, req.user.id, {
        visible: req.body.visible,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async changeState(req, res, next) {
    const { id } = req.params;
    try {
      await req.asyncValidationErrors();
      const data = await pubModel.updatePubUserById(id, {
        state: req.body.state,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async destroy(req, res, next) {
    try {
      const { id } = req.params;
      const data = await pubModel.destroy(id, req.user.id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }

  static async getAllByUserId(req, res, next) {
    try {
      const data = await pubModel.getAllByUserId(req.user.id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async getByCategoriesId(req, res, next) {
    try {
      const { id } = req.params;
      if (!_.isEmpty(id)) {
        const data = await pubModel.getByCategoriesId(id);
        return responseHandler(res, 'SUCCESS', data);
      }
      throw new ValidationError('Id not Defined');
    } catch (err) {
      return next(err);
    }
  }
}
