import Models from '../../../../db/models';
import { ACTIVE } from '../../../constants/article';
import { CONFIRM } from '../../../constants/publications';
import { EDITOR } from '../../../constants/user';

export default class PublicationsModelService {
  static async getAllPublications() {
    try {
      const pub = Models.pubUser.findAll({
        where: { removed: false, status: true },
        include: [
          {
            model: Models.publications,
            raw: true,
          },
        ],
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async create(data) {
    try {
      const pub = Models.publications.create(data);
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async update(id, userId, data) {
    try {
      const update = Models.publications.update(data, {
        where: { id, userId, removed: false },
      });
      return update;
    } catch (err) {
      throw err;
    }
  }
  static async getById(id) {
    try {
      const user = await Models.publications.findOne({
        where: { id, removed: false },
        include: [
          {
            model: Models.favorite,
            required: false,
            attributes: ['pubId'],
            as: 'favoriteItem',
            raw: true,
          },
          {
            required: false,
            model: Models.article,
            as: 'articles',
            where: { type: ACTIVE },
            attributes: [
              'id',
              'name',
              'description',
              'createdAt',
              [
                Models.sequelize.literal(
                  '(SELECT COUNT(`feedback`.`like`) FROM `feedback` WHERE `feedback`.`articleId` = `articles`.`id` AND `feedback`.`like` = 1 )',
                ),
                'like',
              ],
              [
                Models.sequelize.literal(
                  '(SELECT COUNT(`articleComment`.`id`) FROM `articleComment` WHERE `articleComment`.`articleId` = `articles`.`id` AND `articleComment`.`removed` = 0 )',
                ),
                'commentsCount',
              ],
              [
                Models.sequelize.literal(
                  '(SELECT COUNT(`feedback`.`dislike`) FROM `feedback` WHERE `feedback`.`articleId` = `articles`.`id` AND `feedback`.`dislike` = 1 )',
                ),
                'dislike',
              ],
            ],
            include: [
              {
                required: false,
                model: Models.feedback,
                as: 'feedbacks',
                attributes: ['userId', 'like', 'dislike'],
              },
              {
                model: Models.articleImages,
                attributes: ['filename', 'originalName'],
                required: false,
              },

              {
                model: Models.articleAudios,
                attributes: ['filename'],
                required: false,
              },
            ],
          },
        ],
      });
      return user;
    } catch (err) {
      throw err;
    }
  }
  static async getAllByUserId(userId) {
    try {
      const pub = Models.pubUser.findAll({
        where: {
          userId,
          removed: false,
          [Models.sequelize.Op.or]: [{ state: CONFIRM }, { role: EDITOR }],
        },
        include: [
          {
            model: Models.publications,
          },
        ],
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async getByUserId(userId) {
    try {
      const pub = Models.pubUser.findOne({
        where: {
          userId,
          removed: false,
          // status: true,
        },
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async getByUserIdAndRole(userId, pubId, role) {
    try {
      const pub = Models.pubUser.findOne({
        where: {
          userId,
          pubId,
          role,
          removed: false,
          // status: true,
        },
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async getByUserIdAndPubId(userId, pubId) {
    try {
      const pub = Models.pubUser.findOne({
        where: {
          userId,
          pubId,
          removed: false,
          // status: true,
        },
        row: true,
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async getAllByUserIdAndPubId(userId, pubId) {
    try {
      const pub = Models.pubUser.findAll({
        where: {
          userId,
          pubId,
          removed: false,
          // status: true,
        },
        row: true,
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async getByCategoriesId(catId) {
    try {
      const pub = Models.publications.findAll({
        where: { removed: false, catId },
      });
      return pub;
    } catch (err) {
      throw err;
    }
  }
  static async createPubUser(data, transaction = null) {
    try {
      const res = await Models.pubUser.create(data, { transaction });
      return res;
    } catch (err) {
      throw err;
    }
  }
  static async getPubIdAndUserId(pubId, userId) {
    try {
      const res = await Models.pubUser.findOne({
        where: { pubId, userId },
      });
      return res;
    } catch (err) {
      throw err;
    }
  }
  static async updatePubUserById(id, data) {
    try {
      const update = Models.pubUser.update(data, {
        where: { id, removed: false },
      });
      return update;
    } catch (err) {
      throw err;
    }
  }
  static async destroy(id, userId) {
    try {
      const data = await Models.publications.destroy(
        { removed: true },
        { where: { id, userId } },
      );
      return data;
    } catch (err) {
      throw err;
    }
  }
}
