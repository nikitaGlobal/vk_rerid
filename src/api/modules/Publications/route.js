import container from './controller';
import { checkAuthenticationToken } from '../../helpers/middleware/auth';

export default router => {
  router.use(checkAuthenticationToken);
  router.get('/', container.allPublications);
  router.get('/editions', container.getAllByUserId);
  router.post('/create', container.create);
  router.post('/invite-user', container.invitePubUser);
  router.get('/popular/:id', container.getByCategoriesId);
  router.get('/id/:id', container.getPublicationsById);
  router.put('/update', container.update);
  router.put('/state/:id', container.changeState);
  router.put('/visible/:id', container.changeVisible);
  router.delete('/destroy/:id', container.destroy);
};
