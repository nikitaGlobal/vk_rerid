import crypto from 'crypto';
import jwt from 'jsonwebtoken';
import uuid from 'uuid/v4';
import _ from 'lodash';
import Models from '../../../../db/models';
import sendEmail from '../../helpers/email';
import { AuthError, ValidationError } from '../../helpers/errors';
import { ADMINISTRATOR } from '../../../constants/user';
import { CONFIRM } from '../../../constants/publications';

export default class UserModelService {
  static async signIn(email, password) {
    try {
      const temp = {};
      let isConfirmed = true;
      const user = await Models.users.findOne({
        where: { email },
        raw: true,
      });
      if (user) {
        if (user.removed) {
          throw new AuthError('Администратор сайта заблокирует вас');
        }
        if (!_.isEmpty(user.confirm)) {
          isConfirmed = false;
          sendEmail(email, 'email', {
            key: user.confirm,
          });
        }

        if (
          user.password !==
          crypto
            .createHash('sha1')
            .update(password)
            .digest('hex')
        ) {
          throw new AuthError('Неверный логин или пароль');
        }
        const accessToken = jwt.sign(
          {
            id: user.id,
            email: user.email,
          },
          process.env.JWT_SECRET,
          {
            expiresIn: process.env.ACCESS_TOKEN_TTL,
          },
        );
        const refreshToken = `${uuid()}-${uuid()}`;
        await Models.users.update({ refreshToken }, { where: { id: user.id } });

        temp.isConfirmed = isConfirmed;
        temp.token = accessToken;
        temp.refreshToken = refreshToken;
        temp.role = user.role;
        return temp;
      }
      throw new AuthError('Неверный логин или пароль');
    } catch (error) {
      throw error;
    }
  }
  static async singUp(data) {
    try {
      const result = await Models.users.findOne({
        where: { email: data.email },
        raw: true,
      });
      if (!_.isEmpty(result)) {
        throw new ValidationError('Email должен быть уникальным');
      }
      data.password = crypto
        .createHash('sha1')
        .update(data.password)
        .digest('hex');
      data.confirm = crypto
        .createHash('sha1')
        .update(data.email)
        .digest('hex');
      const user = await Models.users.create(data);
      if (user) {
        // const registerToken = jwt.sign(
        //   {
        //     id: user.id,
        //     email: user.email,
        //   },
        //   process.env.JWT_SECRET,
        //   {
        //     expiresIn: process.env.ACCESS_TOKEN_TTL,
        //   },
        // );
        return user;
      }
      throw new ValidationError('Неверный логин или пароль');
    } catch (error) {
      throw error;
    }
  }
  static async update(id, data) {
    try {
      const result = await Models.users.update(data, { where: { id } });
      return result;
    } catch (error) {
      throw error;
    }
  }
  static async getById(id) {
    try {
      const user = await Models.users.findOne({
        where: { id },
        attributes: [
          'id',
          'name',
          'email',
          'firebaseToken',
          'confirm',
          'password',
          'refreshToken',
        ],
        raw: true,
      });
      return user;
    } catch (error) {
      throw error;
    }
  }
  static async getByUserId(id) {
    try {
      const user = await Models.users.findOne({
        where: { id },
        attributes: [
          'id',
          'name',
          'email',
          'avatar',
          'description',
          'confirm',
          'refreshToken',
        ],
        include: [
          {
            required: false,
            model: Models.comment,
            where: { status: false },
            attributes: ['id', 'comment', 'status'],
            as: 'comment',
          },
          {
            required: false,
            model: Models.pubUser,
            where: { removed: false, state: CONFIRM },
            include: [
              {
                model: Models.publications,
                where: { removed: false },
                attributes: ['id', 'name', 'description', 'avatar', 'visible'],
              },
            ],
          },
        ],
      });
      return user;
    } catch (error) {
      throw error;
    }
  }
  static async IsLoggedInToken(token) {
    try {
      const decoded = jwt.decode(token, process.env.JWT_SECRET);
      if (decoded) {
        const user = await Models.users.findOne({
          where: { id: decoded.id },
          attributes: [
            'id',
            'name',
            'email',
            'avatar',
            'description',
            'role',
            'confirm',
            'refreshToken',
          ],
          include: [
            {
              required: false,
              model: Models.comment,
              where: { status: false },
              attributes: ['id', 'comment', 'status'],
              as: 'comment',
              // include: [
              //   {
              //     required: false,
              //     model: Models.answerComments,
              //     as: 'answers',
              //   },
              // ],
            },
            {
              required: false,
              model: Models.publications,
              where: { removed: false },
              attributes: ['id', 'name', 'description', 'avatar', 'visible'],
            },
          ],
        });
        if (!_.isEmpty(user)) {
          return user;
        }
        throw new AuthError('User Not Exist');
      }
      throw new AuthError('Token Not Valid');
    } catch (error) {
      throw error;
    }
  }
  static async checkSignInUser(id) {
    try {
      const user = await Models.users.findOne({
        where: { id },
        attributes: ['id', 'name', 'email', 'role', 'confirm'],
        raw: true,
      });
      return user;
    } catch (error) {
      throw error;
    }
  }
  static async refreshToken(accessToken, refreshToken) {
    try {
      const decoded = jwt.decode(accessToken, process.env.JWT_SECRET);
      const temp = await Models.User.findOne({
        where: { id: decoded.id },
        raw: true,
      });
      if (temp.refreshToken === refreshToken) {
        const newRefreshToken = `${uuid()}-${uuid()}`;
        await Models.User.update(
          { refreshToken: newRefreshToken },
          { where: { id: temp.id } },
        );
        const newAccessToken = jwt.sign(
          {
            id: temp.id,
            email: temp.email,
          },
          process.env.JWT_SECRET,
          {
            expiresIn: process.env.ACCESS_TOKEN_TTL,
          },
        );
        const data = {
          refreshToken: newRefreshToken,
          accessToken: newAccessToken,
        };
        return data;
      }
      throw new AuthError('Неправильный токен обновления');
    } catch (error) {
      throw error;
    }
  }
  static async getUserByMail(data) {
    try {
      const user = await Models.users.findOne({ where: data, raw: true });
      return user;
    } catch (error) {
      throw error;
    }
  }
  static async getUserByPhone(phone) {
    try {
      const user = await Models.users.findOne({
        where: { phone },
        attributes: ['id', 'email', 'type', 'phone'],
        raw: true,
      });
      if (!_.isEmpty(user)) {
        const registerToken = jwt.sign(
          {
            id: user.id,
            email: user.email,
          },
          process.env.JWT_SECRET,
          {
            expiresIn: process.env.ACCESS_TOKEN_TTL,
          },
        );
        return { user, registerToken };
      }
      throw new AuthError('Пользователь не существует');
    } catch (error) {
      throw error;
    }
  }
  static async getUserByKey(confirm) {
    try {
      const temp = await Models.users.findOne({
        where: { confirm, removed: false },
      });
      return temp;
    } catch (error) {
      throw error;
    }
  }
  static async getUserByEmail(email) {
    try {
      const user = await Models.users.findOne({
        where: { email },
        attributes: ['id', 'name', 'email', 'role'],
        raw: true,
      });
      return user;
    } catch (error) {
      throw error;
    }
  }
  static async getAllUsers() {
    try {
      const user = await Models.users.findAll({
        where: {
          removed: false,
          role: { [Models.sequelize.Op.not]: ADMINISTRATOR },
        },
      });
      return user;
    } catch (error) {
      throw error;
    }
  }
}
