import crypto from 'crypto';
import _ from 'lodash';
import responseHandler from '../../../response';
import userModel from './model';
import Notifications from '../../helpers/lib/notifications';
import { ValidationError, BadRequest, AuthError } from '../../helpers/errors';

export default class UserController {
  static async user(req, res, next) {
    try {
      return responseHandler(res, 'SUCCESS', { user: req.user });
    } catch (error) {
      return next(error);
    }
  }
  static async getAllUser(req, res, next) {
    try {
      const data = await userModel.getAllUsers();
      const item = data.filter(x => x.id !== req.user.id);
      return responseHandler(res, 'SUCCESS', item);
    } catch (error) {
      return next(error);
    }
  }
  static async getUserById(req, res, next) {
    const { id } = req.params;
    try {
      const data = await userModel.getByUserId(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (error) {
      return next(error);
    }
  }
  static async updateUser(req, res, next) {
    // req
    //   .checkBody(req.body.value, `${req.body.value} is required.`)
    //   .notEmpty()
    //   .trim();
    try {
      // await req.asyncValidationErrors();
      await userModel.update(req.user.id, req.body);
      return responseHandler(res, 'UPDATE_USER_SUCCESS', { user: req.user });
    } catch (error) {
      return next(error);
    }
  }
  static async updateUserPassword(req, res, next) {
    req
      .checkBody('currentPass', 'currentPass is required.')
      .notEmpty()
      .trim();
    req
      .checkBody('password', 'password is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const user = await userModel.getById(req.user.id);
      if (
        user &&
        user.password ===
          crypto
            .createHash('sha1')
            .update(req.body.currentPass)
            .digest('hex')
      ) {
        const password = crypto
          .createHash('sha1')
          .update(req.body.password)
          .digest('hex');
        await userModel.update(user.id, { password });
        return responseHandler(res, 'UPDATE_USER_SUCCESS', true);
      }
      throw new ValidationError('Текущий пароль не действителен');
    } catch (error) {
      return next(error);
    }
  }
  static async sendNotifications(req, res, next) {
    req
      .checkBody('userId', 'userId is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const user = await userModel.getById(req.body.userId);
      if (!_.isEmpty(user.firebaseToken)) {
        const data = await Notifications.sendNotification(
          user.firebaseToken,
          req.body.title,
          req.body.body,
          req.body.data,
        );
        return responseHandler(res, 'SUCCESS', data.data);
      }
      throw new BadRequest('User have not Firebase Token');
    } catch (error) {
      return next(error);
    }
  }
  static async passwordVerify(req, res, next) {
    req
      .checkBody('password', 'Password cannot be blank.')
      .notEmpty()
      .len({ min: 6 })
      .withMessage('Password must be longer than 6 characters.');
    try {
      const user = await userModel.getById(req.user.id);
      if (!_.isEmpty(user)) {
        const password = crypto
          .createHash('sha1')
          .update(req.body.password)
          .digest('hex');
        await userModel.update(user.id, { confirm: null, password });
        return responseHandler(res, 'SUCCESS');
      }
      throw new AuthError('User not Exist');
    } catch (error) {
      return next(error);
    }
  }
  static async logout(req, res, next) {
    try {
      await userModel.update(req.user.id, { refreshToken: '' });
      return responseHandler(res, 'SUCCESS');
    } catch (error) {
      return next(error);
    }
  }
}
