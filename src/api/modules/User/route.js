import container from './controller';
import { checkAuthenticationToken } from '../../helpers/middleware/auth';

export default router => {
  router.use(checkAuthenticationToken);
  router.get('/', container.getAllUser);
  router.get('/me', container.user);
  router.get('/id/:id', container.getUserById);
  router.put('/update', container.updateUser);
  router.post('/sendNotifications', container.sendNotifications);
  router.put('/update-pass', container.updateUserPassword);
  router.put('/forgot-pass', container.passwordVerify);
  router.post('/logout', container.logout);
};
