import { feedback, articleComment, users } from '../../../../db/models';

module.exports = server => {
  const options = {
    allowUpgrades: true,
    transports: ['polling', 'websocket'],
    pingTimeout: 9000,
    pingInterval: 3000,
    secure: true,
    cookie: 'mycookie',
    httpCompression: true,
    origins: '*:*',
  };
  const io = require('socket.io')(server, options);
  const socketIdToNames = {};
  function socketIdsInRoom(roomId) {
    const socketIds = io.nsps['/socket'].adapter.rooms[roomId];
    if (socketIds) {
      const collection = [];
      // eslint-disable-next-line no-restricted-syntax
      for (const key in socketIds.sockets) {
        collection.push(key);
      }
      return collection;
    }
    return [];
  }
  const nsp = io.of('/socket');
  nsp.on('connection', socket => {
    console.log('Connection');
    socket.on('disconnect', () => {
      console.log('Disconnect');
      delete socketIdToNames[socket.id];
      if (socket.room) {
        io.to(socket.room).emit('leave', socket.id);
        socket.leave(socket.room);
      }
    });
    socket.on('join', data => {
      const roomId = data.id;
      socket.join(roomId);
      socket.room = roomId;
      socketIdToNames[socket.id] = data.name;
    });
    socket.on('comment', async data => {
      const socketIds = socketIdsInRoom(socket.room);
      const sockets = socketIds
        .map(socketId => ({
          socketId,
          name: socketIdToNames[socketId],
        }))
        .filter(x => x.socketId !== socket.id);

      const article = await articleComment.create({
        userId: data.userId,
        articleId: data.articleId,
        comment: data.comment,
      });
      const result = await articleComment.findOne({
        where: { id: article.id, removed: false },
        attributes: ['id', 'comment', 'createdAt'],
        include: [
          {
            required: false,
            model: users,
            attributes: ['id', 'name', 'avatar'],
            where: { removed: false },
          },
        ],
      });

      sockets.forEach(route => {
        io.to(route.socketId).emit('comment', result);
      });
    });
    socket.on('feedback', data => {
      feedback
        .findOne({ where: { userId: data.userId, articleId: data.articleId } })
        .then(async res => {
          if (res) {
            const info = res.get({ plain: true });

            await feedback.destroy({
              where: { id: info.id },
            });
          }
          const result = await feedback.create({
            userId: data.userId,
            articleId: data.articleId,
            like: data.like,
            dislike: data.dislike,
          });
          const socketIds = socketIdsInRoom(socket.room);
          const sockets = socketIds
            .map(socketId => ({
              socketId,
              name: socketIdToNames[socketId],
            }))
            .filter(x => x.socketId !== socket.id);
          sockets.forEach(route => {
            io.to(route.socketId).emit('feedback', result);
          });
        })
        .catch(() => {
          socket.emit('error', { message: 'Error' });
        });
    });
  });
};
