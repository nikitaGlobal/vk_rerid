import _ from 'lodash';
import responseHandler from '../../../response';
import categoriesModel from './model';
import { ValidationError } from '../../helpers/errors';

export default class CategoriesController {
  static async allCategories(req, res, next) {
    try {
      const data = await categoriesModel.getAllCategories();
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async getCategoriesById(req, res, next) {
    try {
      const { id } = req.params;
      if (_.isEmpty(id)) {
        throw new ValidationError('Id not Defined');
      }
      const data = await categoriesModel.getById(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async create(req, res, next) {
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await categoriesModel.create(req.body);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async update(req, res, next) {
    req
      .checkBody('name', 'name is required.')
      .notEmpty()
      .trim();
    try {
      await req.asyncValidationErrors();
      const data = await categoriesModel.update(req.body.id, {
        name: req.body.name,
        avatar: req.body.avatar,
      });
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
  static async destroy(req, res, next) {
    try {
      const { id } = req.params;
      const data = await categoriesModel.destroy(id);
      return responseHandler(res, 'SUCCESS', data);
    } catch (err) {
      return next(err);
    }
  }
}
