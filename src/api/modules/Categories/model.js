import Models from '../../../../db/models';

export default class CategoriesModelService {
  static async getAllCategories() {
    try {
      const cat = Models.categories.findAll({
        where: { removed: false },
        // raw: true,
        include: [
          {
            required: false,
            model: Models.publications,
            row: true,
            as: 'pob',
            attributes: [
              // 'id', // this is OfficeLocation.id, see 4th item above.
              [
                Models.sequelize.fn(
                  'COUNT',
                  Models.sequelize.col('`pob`.`catId`'),
                ),
                'Count',
              ],
            ],
          },
        ],
        group: [`id`],
      });
      return cat;
    } catch (err) {
      throw err;
    }
  }
  static async create(data) {
    try {
      const cat = Models.categories.create(data);
      return cat;
    } catch (err) {
      throw err;
    }
  }
  static async update(id, data) {
    try {
      const update = Models.categories.update(data, { where: { id } });
      return update;
    } catch (err) {
      throw err;
    }
  }
  static async getById(id) {
    try {
      const user = await Models.categories.findOne({
        where: { id, removed: false },
        raw: true,
      });
      return user;
    } catch (err) {
      throw err;
    }
  }
  static async destroy(id) {
    try {
      const data = await Models.categories.update(
        { removed: true },
        { where: { id } },
      );
      return data;
    } catch (err) {
      throw err;
    }
  }
}
