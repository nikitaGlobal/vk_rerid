import container from './controller';
import {
  checkRoleUser,
  checkAuthenticationToken,
} from '../../helpers/middleware/auth';
import * as permission from '../../../constants/user';

export default router => {
  router.use(checkAuthenticationToken);
  router.get('/', container.allCategories);
  router.post(
    '/create',
    // checkRoleUser(permission.ADMINISTRATOR),
    container.create,
  );
  router.get(
    '/id/:id',
    checkRoleUser(permission.ADMINISTRATOR),
    container.getCategoriesById,
  );
  router.put(
    '/update',
    checkRoleUser(permission.ADMINISTRATOR),
    container.update,
  );
  router.delete(
    '/destroy/:id',
    checkRoleUser(permission.ADMINISTRATOR),
    container.destroy,
  );
};
