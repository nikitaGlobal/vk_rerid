import { google } from 'googleapis';
import axios from 'axios';
import notification from './notifications.json';
import { BadRequest } from '../errors';

export default class Notifications {
  static getAccessToken() {
    return new Promise((resolve, reject) => {
      const jwtClient = new google.auth.JWT(
        notification.client_email,
        null,
        notification.private_key,
        'https://www.googleapis.com/auth/firebase.messaging',
        null,
      );
      jwtClient.authorize((err, tokens) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(tokens.access_token);
      });
    });
  }
  static async sendNotification(firebaseToken, title, body, data) {
    try {
      const accessToken = await Notifications.getAccessToken();
      const response = await axios({
        method: 'POST',
        url: `https://fcm.googleapis.com/v1/projects/${notification.project_id}/messages:send`,
        data: {
          message: {
            token: firebaseToken,
            notification: {
              title,
              body,
            },
            data,
          },
        },
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`,
        },
      });
      return response;
    } catch (error) {
      console.log('error', error.response.data);
      throw new BadRequest('Unable to send message to Firebase');
    }
  }
}
