import _ from 'lodash';
import User from '../../modules/User/model';
import pubModel from '../../modules/Publications/model';
import { AuthError } from '../errors';
import responseHandler from '../../../response';
import jwt from 'jsonwebtoken';

export async function checkJwtToken(req, res, next) {
  const authorizationHeader = req.headers.authorization;
  if (authorizationHeader) {
    jwt.verify(
      authorizationHeader.substring(7),
      process.env.JWT_SECRET,
      // eslint-disable-next-line consistent-return
      jwtErr => {
        if (jwtErr) {
          return responseHandler(res, 'Forbidden', { code: 'invalidToken' });
        }
        next();
      },
    );
  } else {
    next();
  }
}
export async function checkAuthenticationToken(req, res, next) {
  try {
    const authorizationHeader = req.headers.authorization;
    if (!authorizationHeader || authorizationHeader.indexOf('Bearer ') === -1) {
      return responseHandler(res, 'Forbidden');
    }
    const user = await User.IsLoggedInToken(authorizationHeader.substring(7));
    if (_.isEmpty(user.refreshToken) || !_.isEmpty(user.confirm)) {
      return responseHandler(res, 'Forbidden');
    }
    req.IsLoggedIn = true;
    req.user = user;
    return next();
  } catch (error) {
    req.user = null;
    req.IsLoggedIn = false;
    return next(new AuthError('Token is  Invalid'));
  }
}
export function checkRoleUser(role) {
  if (typeof role !== 'string') {
    return (req, res, next) => {
      if (role.includes(req.user.role)) {
        return next();
      }
      return next(new AuthError('Haven`t permission'));
    };
  }
  return (req, res, next) => {
    if (req.user.role === role) {
      return next();
    }
    return next(new AuthError('Haven`t permission'));
  };
}
function contains(target, pattern) {
  let value = 0;
  pattern.forEach(word => {
    value += target.includes(word);
  });
  return value === 1;
}
export function checkPermissionUser(role) {
  if (typeof role === 'string') {
    return async (req, res, next) => {
      const user = await pubModel.getByUserIdAndPubId(
        req.user.id,
        req.body.pubId,
      );
      const roleUser = user.map(x => x.role);
      if (!_.isEmpty(user) && contains(role, roleUser)) {
        return next();
      }
      return next(new AuthError('Haven`t permission'));
    };
  }
  return async (req, res, next) => {
    const user = await pubModel.getAllByUserIdAndPubId(
      req.user.id,
      req.body.pubId,
    );
    const roleUser = user.map(x => x.role);
    if (!_.isEmpty(user) && contains(role, roleUser)) {
      return next();
    }
    return next(new AuthError('Haven`t permission'));
  };
}
export function NotSignin(req, res, next) {
  if (!req.IsSignin) {
    next();
  } else {
    return next(new AuthError('you are signin'));
  }
}
export function requireLogin(req, res, next) {
  if (req.IsLoggedIn) {
    return next();
  }
  return responseHandler(res, 'AuthError');
}
