import nodemailer from 'nodemailer';
import fs from 'fs';
import { google } from 'googleapis';

// eslint-disable-next-line prefer-destructuring
const OAuth2 = google.auth.OAuth2;
const oauth2Client = new OAuth2(
  '829153876707-hquboog3qgl2o1cmo9ci6eqoe71cg3qi.apps.googleusercontent.com', // ClientID
  '2qurL99lIyxa03CTZ604W-Hf', // Client Secret
  'https://developers.google.com/oauthplayground', // Redirect URL
);
oauth2Client.setCredentials({
  refresh_token:
    '1//04w-Baz7A9gsJCgYIARAAGAQSNwF-L9Ire8HAlqcO6MxNBToAemcFylUZGuTrfLFVyrTS6cjwnw2FvdaVR1WdG5WeD9IQOHjhBoI',
});
let tokens;
oauth2Client.getRequestHeaders().then(value => {
  tokens = value;
});
//const accessToken = tokens;
// const transporter = nodemailer.createTransport({
//   service: 'gmail',
//   auth: {
//     type: 'OAuth2',
//     user: 'reridtest@gmail.com',
//     clientId:
//       '829153876707-hquboog3qgl2o1cmo9ci6eqoe71cg3qi.apps.googleusercontent.com',
//     clientSecret: '2qurL99lIyxa03CTZ604W-Hf',
//     refreshToken:
//       '1//04w-Baz7A9gsJCgYIARAAGAQSNwF-L9Ire8HAlqcO6MxNBToAemcFylUZGuTrfLFVyrTS6cjwnw2FvdaVR1WdG5WeD9IQOHjhBoI',
//     accessToken,
//   },
// });

 const transporter = nodemailer.createTransport({
  host: 'smtp.beget.ru',
  port: 25,
  secure: false, // true for 465, false for other ports
  from: 'Rerid.com <inbox@rerid.com>',
  auth: {
    user: 'inbox@rerid.com', // generated ethereal user
    pass: '%2ApsLjeHmuj!Zz', // generated ethereal password
  },
   debug: true, // show debug output
   logger: true, // log information in console
});
// const transporter = nodemailer.createTransport({
//   host: 'smtp.beget.ru',
//   port: 2525,
//   secure: false, // true for 465, false for other ports
//   auth: {
//     user: 'inbox@rerid.com', // generated ethereal user
//     pass: '%2ApsLjeHmuj!Zz', // generated ethereal password
//   },
//   // debug: true, // show debug output
//   // logger: true, // log information in console
// });
transporter.verify((error, success) => {
  if (error) {
    console.log('Server', error);
  } else {
    console.log('Server is ready to take our messages -', success);
  }
});
export default (to, templateName, variables = []) => {
  let templateContent = String(
    fs.readFileSync(`${__dirname}/template/${templateName}.html`),
  );
  const footer = String(fs.readFileSync(`${__dirname}/common/footer.html`));
  templateContent = templateContent.replace(
    RegExp('{--footer--}', 'g'),
    footer,
  );
  Object.keys(variables).forEach(element => {
    templateContent = templateContent.replace(
      RegExp(`{{ ${element} }}`, 'g'),
      variables[element],
    );
  });
  transporter.sendMail(
    {
      from: 'inbox@rerid.com',
      to,
      subject: 'Rerid',
      html: templateContent,
    },
    (error, response) => {
      // callback
      if (error) {
        console.log(error);
      } else {
        console.log(`Message sent: ${response.response}`);
      }
      transporter.close(); // shut down the connection pool, no more messages.  Comment this line out to continue sending emails.
    },
  );
};
