import multer from 'multer';
import path from 'path';
// Set The Storage Engine
const storage = multer.diskStorage({
  destination: 'assets/upload',
  //   path:function(req , file ,cb){
  //     cb(null,req.protocol + '://' + req.get('host') + req.originalUrl +req.filename);
  //   },
  filename(req, file, cb) {
    cb(
      null,
      `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`,
    );
  },
});

// Check File Type
// eslint-disable-next-line consistent-return
const checkFileType = (file, cb) => {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  }
  cb('Error: Images Only!');
};
// Init Upload
export default multer({
  storage,
  limits: { fieldSize: 25 * 1024 * 1024 },
  // limits: { fileSize: 10000000000000000 },
  // fileFilter(req, file, cb) {
  //   checkFileType(file, cb);
  // },
});
