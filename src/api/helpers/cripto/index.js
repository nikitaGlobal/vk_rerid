import Crypto from 'crypto';
import _ from 'lodash';
import rtcModel from '../../modules/Webrtc/rtcModel';

export const urlDecode = encoded => {
  encoded = encoded.replace(/-/g, '+').replace(/_/g, '/');
  while (encoded.length % 4) encoded += '=';
  return encoded;
};
export const decrypt = data => {
  try {
    const decipher = Crypto.createDecipher('aes-256-cbc', 'baby');
    const decrypted = Buffer.concat([
      decipher.update(Buffer.from(urlDecode(data), 'base64')),
      decipher.final(),
    ]);
    return JSON.parse(decrypted.toString());
  } catch (exception) {
    throw new Error(exception.message);
  }
};
export const urlEncode = unencoded =>
  unencoded
    .replace(/\+/g, '-')
    .replace(/\//g, '_')
    .replace(/=+$/, '');

export const encrypt = data => {
  try {
    const cipher = Crypto.createCipher('aes-256-cbc', 'baby');
    const encrypted = Buffer.concat([
      cipher.update(Buffer.from(JSON.stringify(data), 'utf8')),
      cipher.final(),
    ]);
    return urlEncode(encrypted.toString('base64'));
  } catch (exception) {
    throw new Error(exception.message);
  }
};

export const generateCode = async number => {
  const arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  let code = '';
  let index;
  for (let i = 0; i < number; i++) {
    index = Math.floor(Math.random(0, arr.length - 1) * 10);
    code += arr[index];
  }
  const result = await rtcModel.getByGeneRateNumber(code);
  if (!_.isEmpty(result)) {
    generateCode(number);
  }
  return Number(code);
};
