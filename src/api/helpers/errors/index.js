export class NotFound extends Error {
  constructor(
    errorsMessage = null,
    errors = null,
    message = 'NOT_FOUND',
    status = 404,
  ) {
    super();
    this.message = message;
    this.errorsMessage = errorsMessage;
    this.errors = errors;
    this.status = status;
  }
}
export class AuthError extends Error {
  constructor(
    errorsMessage = null,
    errors = null,
    message = 'Unauthorized',
    status = 401,
  ) {
    super();
    this.message = message;
    this.errorsMessage = errorsMessage;
    this.errors = errors;
    this.status = status;
  }
}
export class ValidationError extends Error {
  constructor(
    errorsMessage = null,
    errors = null,
    message = 'VALIDATION_ERROR',
    status = 422,
  ) {
    super();
    this.message = message;
    this.errorsMessage = errorsMessage;
    this.errors = errors;
    this.status = status;
  }
}
export class BadRequest extends Error {
  constructor(
    errorsMessage = null,
    errors = null,
    message = 'BAD_REQUEST',
    status = 400,
  ) {
    super();
    this.message = message;
    this.errorsMessage = errorsMessage;
    this.errors = errors;
    this.status = status;
  }
}

export class Forbidden extends Error {
  constructor(
    errorsMessage = null,
    errors = null,
    message = 'Forbidden',
    status = 403,
  ) {
    super();
    this.message = message;
    this.errorsMessage = errorsMessage;
    this.errors = errors;
    this.status = status;
  }
}
