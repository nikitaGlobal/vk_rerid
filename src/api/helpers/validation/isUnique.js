// import UserModel from '../../modules/admin/user/userModel';
import 'babel-polyfill';

export const isUniqueUserName = async username => {
  try {
    if (!(await UserModel.isUniqueUserName(username))) {
      throw new Error('');
    }
  } catch (error) {
    throw error;
  }
};
export const isUniqueEmail = async email => {
  try {
    if (!(await UserModel.isUniqueEmail(email))) {
      throw new Error('');
    }
  } catch (error) {
    throw error;
  }
};
