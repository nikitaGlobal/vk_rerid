import UsersModule from './modules/User/module';
import ImageModule from './modules/images/imageModule';
import AuthModule from './modules/Auth/authModule';
import CategoriesModule from './modules/Categories/module';
import PublicationsModule from './modules/Publications/module';
import ArticleModule from './modules/Article/module';
import FavoriteModule from './modules/Favorite/module';

export default router => {
  const modules = [];
  const publications = PublicationsModule(router);
  const categories = CategoriesModule(router);
  const article = ArticleModule(router);
  const favorite = FavoriteModule(router);
  const users = UsersModule(router);
  const image = ImageModule(router);
  const auth = AuthModule(router);

  modules.push(publications);
  modules.push(categories);
  modules.push(article);
  modules.push(favorite);
  modules.push(users);
  modules.push(image);
  modules.push(auth);

  modules.forEach(module => {
    module.createEndpoints();
  });
  return router;
};
