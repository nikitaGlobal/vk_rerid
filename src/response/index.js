const response = {
  SUCCESS: {
    status: 200,
    json: {
      status: 'Success',
      message: 'Request was successfully completed.',
      data: {},
      errors: null,
    },
  },
  SIGNED_IN: {
    status: 200,
    json: {
      status: 'Success',
      message: 'User was successfully signed in.',
      data: null,
      errors: null,
    },
  },
  SIGNED_UP: {
    status: 201,
    json: {
      status: 'Success',
      message: 'User was successfully signed up.',
      data: null,
      errors: null,
    },
  },
  SIGNED_OUT: {
    status: 201,
    json: {
      status: 'Success',
      message: 'User was successfully signed out.',
      data: null,
      errors: null,
    },
  },
  CREATE_USER_SUCCESS: {
    status: 200,
    json: {
      status: 'Success',
      message: 'User was successfully created.',
      data: null,
      errors: null,
    },
  },
  UPDATE_USER_SUCCESS: {
    status: 201,
    json: {
      status: 'Success',
      message: 'User was successfully updated.',
      data: null,
      errors: null,
    },
  },
  DELETE_USER_SUCCESS: {
    status: 201,
    json: {
      status: 'Success',
      message: 'User was successfully deleted.',
      data: null,
      errors: null,
    },
  },
  Forbidden: {
    status: 403,
    json: {
      status: 'error',
      message: 'Your access token is invalid.',
      data: null,
      errors: null,
    },
  },
  Unauthorized: {
    status: 401,
    json: {
      status: 'error',
      message: 'Your access token is invalid.',
      data: null,
      errors: null,
    },
  },
};
export default (res, name, data = null, errors = null, message = null) => {
  // eslint-disable-next-line one-var
  let status, json;
  const details = response[name];
  if (details) {
    status = details.status;
    message = !!message ? message : details.json.message;
    json = Object.assign(details.json, {
      data,
      message,
      errors,
    });
  }
  return res.status(status).json(json);
};
