import express from 'express';
// import winston from 'winston';
import logger from 'morgan';
import {
  isUniqueUserName,
  isUniqueEmail,
} from './api/helpers/validation/isUnique';
import compression from 'compression';
import expressValidator from 'express-validator';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import device from 'express-device';
import enableRoutes from './api';
// import { checkJwtToken } from './api/helpers/middleware/auth';

class Application {
  constructor(app, router) {
    this.app = express();
    this.initApp();
  }
  initApp() {
    this.configApp();
    // this.setParams();
    this.setRouter();
    this.setErrorHandler();
  }

  configApp() {
    this.app.use(helmet());
    this.app.use(compression());
    this.app.use(device.capture());
    this.app.use(logger('dev'));
    // this.app.use(checkJwtToken);
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
    this.app.set('trust proxy', 1);
    this.app.use(
      expressValidator({
        errorFormatter: (param, message, value, location) => ({
          param,
          message,
          value,
          location,
        }),
        customValidators: {
          isUniqueEmail,
          isUniqueUserName,
        },
      }),
    );
    this.app.use((req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, PUT, POST, DELETE, OPTIONS',
      );
      res.setHeader(
        'Access-Control-Allow-Headers',
        'Content-Type, Authorization',
      );
      next();
    });
    this.app.options('*', (req, res, next) => {
      res.header('Access-Control-Allow-Origin', req.get('Origin') || '*');
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization',
      );
      // other headers here
      res.status(200).end();
    });
  }
  setRouter() {
    this.router = express.Router();
    this.app.use('/api', enableRoutes(this.router));
    this.app.use((req, res, next) => {
      const err = new Error('Not Found');
      err.status = 404;
      return next(err);
      // res.send('not working dis page');
    });
  }

  setErrorHandler() {
    this.app.use((error, req, res, next) => {
      console.log('error', error);
      if (error instanceof Array) {
        return res.status(error.status || 400).json({
          status: 'Error',
          message: 'VALIDATION_ERROR',
          data: null,
          errors: error[0].message,
        });
      }
      if (error.status) {
        return res.status(error.status).json({
          status: 'Error',
          error: error.message,
          message: error.errorsMessage,
          data: null,
          // errors: error.errors,
        });
      }
      return res.status(500).json({
        status: 'Error',
        message: error.message,
        data: null,
        errors: error.errors,
      });
    });
  }
}

export default () => new Application().app;
