module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('articleAudios', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      filename: {
        allowNull: false,
        type: Sequelize.STRING(200),
      },
      artId: {
        allowNull: true,
        type: Sequelize.UUID,
        tableName: 'article',
        referencesKey: 'id',
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('articleAudios'),
};
