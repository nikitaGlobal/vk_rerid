module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('users', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      email: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING,
      },
      avatar: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.TEXT,
      },
      publishers: {
        type: Sequelize.STRING,
      },
      role: {
        type: Sequelize.ENUM('1', '2'),
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      refreshToken: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      confirm: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      firebaseToken: {
        allowNull: true,
        type: Sequelize.STRING,
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('users'),
};
