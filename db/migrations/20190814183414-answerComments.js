module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('answerComments', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      comment: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      from: {
        allowNull: false,
        type: Sequelize.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      to: {
        allowNull: false,
        type: Sequelize.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      comId: {
        allowNull: false,
        type: Sequelize.UUID,
        tableName: 'comment',
        referencesKey: 'id',
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('answerComments'),
};
