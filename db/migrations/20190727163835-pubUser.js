module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('pubUser', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      userId: {
        allowNull: false,
        type: Sequelize.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      pubId: {
        allowNull: true,
        type: Sequelize.UUID,
        tableName: 'publications',
        referencesKey: 'id',
      },
      role: {
        allowNull: false,
        type: Sequelize.ENUM('2', '4', '8', '16'),
      },
      status: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('articleUser'),
};
