module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('article', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(150),
      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      pubId: {
        allowNull: true,
        type: Sequelize.UUID,
        tableName: 'publications',
        referencesKey: 'id',
      },
      status: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('article'),
};
