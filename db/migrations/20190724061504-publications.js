module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('publications', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING(150),
      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      avatar: {
        allowNull: true,
        type: Sequelize.STRING(150),
      },
      userId: {
        allowNull: true,
        type: Sequelize.UUID,
        tableName: 'user',
        referencesKey: 'id',
      },
      catId: {
        allowNull: true,
        type: Sequelize.UUID,
        tableName: 'categories',
        referencesKey: 'id',
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('publications'),
};
