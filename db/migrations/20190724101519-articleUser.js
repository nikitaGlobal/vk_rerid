module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.createTable('articleUser', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      userId: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      artId: {
        allowNull: true,
        type: Sequelize.UUID,
        tableName: 'article',
        referencesKey: 'id',
      },
      role: {
        allowNull: false,
        type: Sequelize.ENUM('2', '4', '8'),
      },
      removed: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }),
  down: queryInterface => queryInterface.dropTable('articleUser'),
};
