module.exports = (sequelize, DataTypes) => {
  const articleImages = sequelize.define(
    'articleImages',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      filename: {
        allowNull: false,
        type: DataTypes.STRING(150),
      },
      originalName: {
        allowNull: false,
        type: DataTypes.STRING(150),
      },
      artId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'article',
        referencesKey: 'id',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  articleImages.associate = function(models) {
    articleImages.belongsTo(models.article, { foreignKey: 'artId' });
    // associations can be defined here
  };
  return articleImages;
};
