/* eslint-disable func-names */
module.exports = (sequelize, DataTypes) => {
  const article = sequelize.define(
    'article',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(150),
        defaultValue: '',
      },
      description: {
        type: DataTypes.TEXT,
        defaultValue: null,
      },
      pubId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'publications',
        referencesKey: 'id',
      },
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      type: {
        type: DataTypes.ENUM('1', '2', '3'),
        defaultValue: '1',
      },
      userId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  article.associate = function(models) {
    article.belongsTo(models.publications, {
      foreignKey: 'pubId',
      as: 'articles',
    });

    article.hasMany(models.articleUser, {
      foreignKey: 'artId',
      onDelete: 'CASCADE',
    });
    article.hasMany(models.articleImages, {
      foreignKey: 'artId',
      onDelete: 'CASCADE',
    });
    article.hasMany(models.articleAudios, {
      foreignKey: 'artId',
      onDelete: 'CASCADE',
    });
    article.hasMany(models.feedback, {
      foreignKey: 'articleId',
      as: 'feedbacks',
      onDelete: 'CASCADE',
    });
    article.hasMany(models.articleComment, {
      foreignKey: 'articleId',
      as: 'comment',
      onDelete: 'CASCADE',
    });
    // article.hasMany(models.comment, { foreignKey: 'artId' });
    // associations can be defined here
  };
  return article;
};
