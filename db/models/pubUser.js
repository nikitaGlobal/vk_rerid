module.exports = (sequelize, DataTypes) => {
  const pubUser = sequelize.define(
    'pubUser',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      userId: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      pubId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'publications',
        referencesKey: 'id',
      },
      role: {
        type: DataTypes.ENUM('2', '4', '8', '16'),
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      state: {
        type: DataTypes.ENUM('1', '2', '3'),
        defaultValue: '1',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  pubUser.associate = function(models) {
    pubUser.belongsTo(models.publications, { foreignKey: 'pubId' });
    pubUser.belongsTo(models.publications, {
      foreignKey: 'pubId',
      as: 'owner_pub',
    });
    pubUser.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'owner',
    });
    pubUser.hasOne(models.comment, {
      foreignKey: 'pubUserId',
      as: 'info',
    });
    pubUser.belongsTo(models.users, { foreignKey: 'userId' });
    // pubUser.belongsTo(models.userId, { foreignKey: 'userId' });
    // pubUser.hasMany(models.users, { foreignKey: 'userId' });
    // associations can be defined here
  };
  return pubUser;
};
