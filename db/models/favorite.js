module.exports = (sequelize, DataTypes) => {
  const Favorite = sequelize.define(
    'favorite',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      userId: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      pubId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'publications',
        referencesKey: 'id',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  Favorite.associate = function(models) {
    Favorite.belongsTo(models.publications, { foreignKey: 'pubId' });
    Favorite.belongsTo(models.publications, {
      foreignKey: 'pubId',
      as: 'favoriteItem',
    });
    Favorite.belongsTo(models.users, { foreignKey: 'userId' });
  };
  return Favorite;
};
