module.exports = (sequelize, DataTypes) => {
  const comment = sequelize.define(
    'comment',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      comment: {
        allowNull: true,
        type: DataTypes.TEXT,
      },
      from: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      to: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      pubUserId: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'pubUser',
        referencesKey: 'id',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  comment.associate = function(models) {
    // comment.belongsTo(models.article, { foreignKey: 'artId' });
    // comment.belongsTo(models.users, { foreignKey: 'to' });
    comment.belongsTo(models.users, {
      foreignKey: 'to',
      as: 'invited_user',
    });
    comment.belongsTo(models.users, {
      foreignKey: 'from',
      as: 'send',
    });
    comment.belongsTo(models.pubUser, {
      foreignKey: 'pubUserId',
      as: 'info',
    });
    comment.hasMany(models.answerComments, {
      foreignKey: 'comId',
      as: 'answers',
    });
    // associations can be defined here
  };
  return comment;
};
