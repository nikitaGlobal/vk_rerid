module.exports = (sequelize, DataTypes) => {
  const categories = sequelize.define(
    'categories',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(150),
        defaultValue: '',
      },
      avatar: {
        type: DataTypes.STRING(150),
        defaultValue: null,
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  categories.associate = function(models) {
    categories.hasMany(models.publications, { foreignKey: 'catId' });
    categories.hasOne(models.publications, {
      foreignKey: 'catId',
      as: 'pob',
      onDelete: 'CASCADE',
    });
    // associations can be defined here
  };
  return categories;
};
