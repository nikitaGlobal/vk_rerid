module.exports = (sequelize, DataTypes) => {
  const publications = sequelize.define(
    'publications',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(150),
        defaultValue: '',
      },
      description: {
        type: DataTypes.TEXT,
        defaultValue: null,
      },
      avatar: {
        type: DataTypes.STRING(150),
        defaultValue: null,
      },
      catId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'categories',
        referencesKey: 'id',
      },
      userId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      visible: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  publications.associate = function(models) {
    publications.belongsTo(models.categories, { foreignKey: 'catId' });
    publications.belongsTo(models.users, { foreignKey: 'userId' });
    publications.belongsTo(models.users, {
      foreignKey: 'userId',
      as: 'publication',
    });
    publications.hasMany(models.article, {
      foreignKey: 'pubId',
      as: 'articles',
    });

    publications.hasMany(models.pubUser, {
      foreignKey: 'pubId',
      as: 'owner_pub',
    });
    publications.hasMany(models.pubUser, { foreignKey: 'pubId' });
    publications.hasMany(models.favorite, { foreignKey: 'pubId' });
    publications.hasOne(models.favorite, {
      foreignKey: 'pubId',
      as: 'favoriteItem',
    });
    // associations can be defined here
  };
  return publications;
};
