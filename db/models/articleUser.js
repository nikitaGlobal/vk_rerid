module.exports = (sequelize, DataTypes) => {
  const articleUser = sequelize.define(
    'articleUser',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      userId: {
        allowNull: false,
        type: DataTypes.UUID,
      },
      artId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'article',
        referencesKey: 'id',
      },
      role: {
        type: DataTypes.ENUM('2', '4', '8'),
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  articleUser.associate = function(models) {
    articleUser.belongsTo(models.article, { foreignKey: 'artId' });
    // associations can be defined here
  };
  return articleUser;
};
