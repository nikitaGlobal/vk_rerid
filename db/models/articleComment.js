module.exports = (sequelize, DataTypes) => {
  const articleComment = sequelize.define(
    'articleComment',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      comment: {
        allowNull: true,
        type: DataTypes.TEXT,
      },
      articleId: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'article',
        referencesKey: 'id',
      },
      userId: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  articleComment.associate = function(models) {
    // comment.belongsTo(models.article, { foreignKey: 'artId' });
    articleComment.belongsTo(models.users, { foreignKey: 'userId' });
    articleComment.belongsTo(models.article, {
      foreignKey: 'articleId',
      // as: 'comment',
    });
  };
  return articleComment;
};
