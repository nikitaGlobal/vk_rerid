module.exports = (sequelize, DataTypes) => {
  const answerComments = sequelize.define(
    'answerComments',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      comment: {
        allowNull: true,
        type: DataTypes.TEXT,
      },
      from: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      to: {
        allowNull: false,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      comId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'comment',
        referencesKey: 'id',
      },
      status: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  answerComments.associate = function(models) {
    answerComments.belongsTo(models.comment, {
      foreignKey: 'comId',
      as: 'answers',
    });
    // comment.belongsTo(models.users, { foreignKey: 'to' });
    // associations can be defined here
  };
  return answerComments;
};
