module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define(
    'users',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(255),
        defaultValue: 'Гость',
      },
      email: {
        type: DataTypes.STRING(255),
        unique: true,
      },
      avatar: {
        type: DataTypes.STRING(255),
        defaultValue: null,
      },
      description: {
        type: DataTypes.TEXT,
      },
      publishers: {
        type: DataTypes.STRING(255),
        defaultValue: null,
      },
      role: {
        type: DataTypes.ENUM('1', '2'),
        defaultValue: '2',
      },
      password: {
        type: DataTypes.STRING,
      },
      confirm: {
        type: DataTypes.STRING(255),
      },
      refreshToken: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      firebaseToken: {
        type: DataTypes.STRING,
        defaultValue: '',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  // eslint-disable-next-line func-names
  Users.associate = function(models) {
    Users.hasMany(models.publications, { foreignKey: 'userId' });
    Users.hasOne(models.pubUser, {
      foreignKey: 'userId',
      as: 'owner',
    });
    Users.hasMany(models.comment, {
      foreignKey: 'to',
      as: 'comment',
    });
    Users.hasOne(models.comment, {
      foreignKey: 'from',
      as: 'send',
    });
    Users.hasMany(models.pubUser, { foreignKey: 'userId' });
    Users.hasMany(models.favorite, { foreignKey: 'userId' });
    Users.hasOne(models.publications, {
      foreignKey: 'userId',
      as: 'publication',
    });
    Users.hasOne(models.articleComment, {
      foreignKey: 'userId',
      as: 'user',
    });
    // associations can be defined here
  };
  return Users;
};
