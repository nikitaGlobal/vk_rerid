module.exports = (sequelize, DataTypes) => {
  const feedback = sequelize.define(
    'feedback',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true,
      },
      like: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      dislike: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      userId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'users',
        referencesKey: 'id',
      },
      articleId: {
        allowNull: true,
        type: DataTypes.UUID,
        tableName: 'article',
        referencesKey: 'id',
      },
      removed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
      updatedAt: {
        type: DataTypes.DATE,
        defaultValue: sequelize.DATE,
      },
    },
    {},
  );
  // eslint-disable-next-line no-unused-vars
  feedback.associate = function(models) {
    feedback.belongsTo(models.feedback, {
      foreignKey: 'articleId',
      as: 'feedbacks',
    });
    // associations can be defined here
  };
  return feedback;
};
